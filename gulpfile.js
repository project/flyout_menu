'use strict';

const gulp = require('gulp');
const plugins = require('gulp-load-plugins')({
  pattern: '*',
  rename: {
    'gulp-postcss': 'postcss',
    'gulp-rename': 'rename',
    'gulp-sass': 'sass',
    'gulp-sass-glob': 'sassGlob',
    'gulp-sourcemaps': 'sourcemaps'
  }
});

function build () {
  return gulp.src('./scss/**/*.scss')
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sassGlob())
    .pipe(plugins.sass({ outputStyle: 'compressed' }))
    .pipe(plugins.postcss([plugins.autoprefixer(), plugins.cssnano()]))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest('./css'));
}

function watch () {
  gulp.watch('./scss/**/*.scss', build);
}

exports.build = build;
exports.watch = watch;
exports.default = gulp.series(build, watch);
